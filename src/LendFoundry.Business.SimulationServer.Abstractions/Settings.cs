﻿using System;

namespace LendFoundry.Business.SimulationServer
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "simulation-server";
        public static string TenantName => Environment.GetEnvironmentVariable($"TENANT_NAME") ?? "my-tenant";
    }
}