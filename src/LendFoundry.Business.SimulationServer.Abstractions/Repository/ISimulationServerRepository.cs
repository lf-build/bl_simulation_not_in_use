﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Business.SimulationServer
{
    public interface ISimulationServerRepository : IRepository<ISimulationServerDocuments>
    {

    }
}
