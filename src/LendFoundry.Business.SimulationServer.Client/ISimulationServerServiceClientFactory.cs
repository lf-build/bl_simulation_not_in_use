﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Business.SimulationServer.Client
{
    public interface ISimulationServerServiceClientFactory
    {
        ISimulationServerService Create(ITokenReader reader);
    }
}
