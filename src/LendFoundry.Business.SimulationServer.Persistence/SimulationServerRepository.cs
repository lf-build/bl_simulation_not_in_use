﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;

namespace LendFoundry.Business.SimulationServer.Persistence
{
    public class SimulationServerRepository : MongoRepository<ISimulationServerDocuments, SimulationServerDocuments>, ISimulationServerRepository
    {
        static SimulationServerRepository()
        {
            BsonClassMap.RegisterClassMap<SimulationServerDocuments>(map =>
            {
                map.AutoMap();
                var type = typeof(SimulationServerDocuments);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public SimulationServerRepository(IMongoConfiguration configuration)
            : base(new FaleTenantService(), configuration, "SimulationServerDocuments")
        {

        }

    }

    public class FaleTenantService : ITenantService
    {
        public TenantInfo Current
        {
            get
            {
                return new TenantInfo() { Id = Settings.TenantName };
            }
        }

        public List<TenantInfo> GetActiveTenants()
        {
            throw new NotImplementedException();
        }
    }
}
