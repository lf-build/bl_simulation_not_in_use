FROM registry.lendfoundry.com/base:beta8

LABEL RevisionNumber "<<RevisionNumber>>"

ADD ./src/LendFoundry.SimulationServer.Abstractions /app/LendFoundry.SimulationServer.Abstractions
WORKDIR /app/LendFoundry.SimulationServer.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build


ADD ./src/LendFoundry.SimulationServer.Persistence /app/LendFoundry.SimulationServer.Persistence
WORKDIR /app/LendFoundry.SimulationServer.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build


ADD ./src/LendFoundry.SimulationServer /app/LendFoundry.SimulationServer
WORKDIR /app/LendFoundry.SimulationServer
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.SimulationServer.Api /app/LendFoundry.SimulationServer.Api
WORKDIR /app/LendFoundry.SimulationServer.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel