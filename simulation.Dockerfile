FROM registry.lendfoundry.com/base:beta8


ADD ./src/LendFoundry.SimulationServer.Abstractions /app/LendFoundry.SimulationServer.Abstractions
WORKDIR /app/LendFoundry.SimulationServer.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://111.93.247.122:8111/guestAuth/app/nuget/v1/FeedService.svc/ -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build


ADD ./src/LendFoundry.SimulationServer.Persistence /app/LendFoundry.SimulationServer.Persistence
WORKDIR /app/LendFoundry.SimulationServer.Persistence
RUN dnu restore --ignore-failed-sources --no-cache -s http://111.93.247.122:8111/guestAuth/app/nuget/v1/FeedService.svc/ -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build


ADD ./src/LendFoundry.SimulationServer /app/LendFoundry.SimulationServer
WORKDIR /app/LendFoundry.SimulationServer
RUN dnu restore --ignore-failed-sources --no-cache -s http://111.93.247.122:8111/guestAuth/app/nuget/v1/FeedService.svc/ -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/LendFoundry.SimulationServer.Api /app/LendFoundry.SimulationServer.Api
WORKDIR /app/LendFoundry.SimulationServer.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://111.93.247.122:8111/guestAuth/app/nuget/v1/FeedService.svc/ -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel